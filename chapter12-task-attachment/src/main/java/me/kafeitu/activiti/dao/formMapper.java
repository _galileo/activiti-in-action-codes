package com.galileo.activiti.dao;

import com.galileo.activiti.bean.form;
import com.galileo.activiti.bean.formExample;
import com.galileo.activiti.bean.formWithBLOBs;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface formMapper {
    int countByExample(formExample example);

    int deleteByExample(formExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(formWithBLOBs record);

    int insertSelective(formWithBLOBs record);

    List<formWithBLOBs> selectByExampleWithBLOBs(formExample example);

    List<form> selectByExample(formExample example);

    formWithBLOBs selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") formWithBLOBs record, @Param("example") formExample example);

    int updateByExampleWithBLOBs(@Param("record") formWithBLOBs record, @Param("example") formExample example);

    int updateByExample(@Param("record") form record, @Param("example") formExample example);

    int updateByPrimaryKeySelective(formWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(formWithBLOBs record);

    int updateByPrimaryKey(form record);
}